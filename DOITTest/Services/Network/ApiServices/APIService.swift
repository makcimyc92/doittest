//
//  APIService.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 20.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import Foundation

import Foundation
import Alamofire
import SwiftyJSON

final class APIService {
    private let apiPath = "http://api.doitserver.in.ua"
    private var jwtToken = ""

    var authToken: String {
        get {
            if jwtToken.isEmpty {
                return UserDefaults.standard.string(forKey: "kToken") ?? ""
            } else {
                return jwtToken
            }
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "kToken");
            jwtToken = newValue
        }
    }
    
    static let service = APIService()
    
    func performRequest(_ requestItem: BaseRequestProtocol) {
        if requestItem.isUpload {
            performUploadRequest(requestItem)
        } else {
            performJSONRequest(requestItem)
        }
    }
    
    private func defaultHeaders() -> [String:String] {
        var headers = [String:String]()
        if !self.authToken.isEmpty {
            headers["token"] = self.authToken
        }
        return headers
    }
    
    private func performJSONRequest(_ requestItem: BaseRequestProtocol) {
        request(apiPath+requestItem.apiPath()!, method: requestItem.method(), parameters: requestItem.parameters(), encoding: requestItem.encoding(), headers: defaultHeaders()).responseJSON(options: .allowFragments, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                requestItem.requestCompleted(.success(r: value as AnyObject))
            case .failure(let error):
                requestItem.requestCompleted(.error(e: error))
            }
        })
    }
    
    private func performUploadRequest(_ requestItem: BaseRequestProtocol) {
        upload(multipartFormData: { (multipartFormData) in
            for (key, value) in requestItem.imageData() {
                multipartFormData.append(value, withName: key, fileName: key, mimeType: value.mimeType)
            }
            for (key, value) in requestItem.formData(){
                multipartFormData.append(value, withName: key)
            }
        }, to: apiPath+requestItem.apiPath()!,
           method:requestItem.method(),
           headers:defaultHeaders(),
           encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .failure(let error):
                        requestItem.requestCompleted(.error(e: error))
                    case .success(let value):
                        requestItem.requestCompleted(.success(r: value as AnyObject))
                    }
                })
            case .failure(let encodingError):
                requestItem.requestCompleted(.error(e: encodingError))
            }
        })
    }

}

