//
//  BaseRequest.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 20.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import Foundation
import Alamofire

class BaseRequest: NSObject, BaseRequestProtocol {
    
    var parametersWithSessionKey: NSMutableDictionary
    var completion  : (ResponseType)->()
    var progress    : (Float)->()
    
    init(completionClosure: @escaping (_ response:ResponseType)->()){
        completion = completionClosure
        progress = { progress in}
        parametersWithSessionKey = NSMutableDictionary()
    }
    
    func method() -> HTTPMethod{
        return .get
    }
    
    func data() -> Data? {
        return nil
    }
    
    func apiPath()->String? {
        return nil
    }
    
    func parameters()-> [String:Any]{
        return [:]
    }
    
    func encoding() -> ParameterEncoding {
        return URLEncoding.default
    }
    
    func formData() -> [String:Data] {
        return [:]
    }
    
    func imageData() -> [String : Data] {
        return [:]
    }
    
    func requestCompleted(_ response: ResponseType) {
        self.completion(response)
    }
    
    func requestProgress(_ progress: Float){
        self.progress(progress)
    }
    
    func perform(){
        APIService.service.performRequest(self)
    }
    
    
}
