//
//  SignUpRequest.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 20.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import Foundation
import Alamofire

class SignUpRequest: BaseRequest {
    var email = ""
    var password = ""
    var username = ""
    var avatar:UIImage?
    
    override func apiPath() -> String? {
        return "/create"
    }
    
    override func method() -> HTTPMethod {
        return .post
    }
    
    override func imageData() -> [String : Data] {
        guard let image = avatar,
            let data = UIImageJPEGRepresentation(image, 0.2) else {
                return [:]
        }
        return ["avatar": data];
    }

    override func formData() -> [String : Data] {
        return ["email":email.data(using: .utf8)!,
                "password":password.data(using: .utf8)!,
                "username":username.data(using: .utf8)!]
    }

}
