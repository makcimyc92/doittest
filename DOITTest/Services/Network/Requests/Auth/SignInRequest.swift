//
//  SignInRequest.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 20.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import Foundation
import Alamofire

class SignInRequest: BaseRequest {

    var email = ""
    var password = ""
    
    override func apiPath() -> String? {
        return "/login"
    }
    
    override func method() -> HTTPMethod {
        return .post
    }
    
    override func parameters() -> [String : Any] {
        return ["email":email,
                "password":password]
    }
}
