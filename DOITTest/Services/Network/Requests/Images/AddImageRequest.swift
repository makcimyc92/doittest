//
//  AddImageRequest.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 9/20/17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import UIKit
import Alamofire

class AddImageRequest: BaseRequest {
    
    var image:UIImage!
    var descriptionImage = ""
    var hashtag = ""
    var latitude = 0.0
    var longitude = 0.0
    
    override func method() -> HTTPMethod {
        return .post
    }
    
    override func apiPath() -> String? {
        return "/image"
    }
    
    override func formData() -> [String : Data] {
        let latitudeString = String(latitude)
        let longitudeString = String(longitude)
        return ["description":descriptionImage.data(using: .utf8)!,
                "hashtag":hashtag.data(using: .utf8)!,
                "latitude":latitudeString.data(using: .utf8)!,
                "longitude":longitudeString.data(using: .utf8)!]
    }
    
    override func imageData() -> [String : Data] {
        guard let data = UIImageJPEGRepresentation(image, 0.2) else {
            return [:]
        }
        return ["image": data];
    }
    
    
}
