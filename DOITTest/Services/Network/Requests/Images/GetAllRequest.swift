//
//  GetAddedImages.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 9/20/17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import UIKit
import Alamofire

class GetAllRequest: BaseRequest {
    
    override func apiPath() -> String? {
        return "/all"
    }
}
