//
//  ShowGifRequest.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 21.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import UIKit

class ShowGifRequest: BaseRequest {
    
    var weather = ""
    
    override func apiPath() -> String? {
        return "/gif"
    }
    
    override func parameters() -> [String : Any] {
        return ["weather":weather]
    }

}
