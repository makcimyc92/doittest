//
//  SignInViewModel.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 20.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import Foundation

import SwiftyJSON

protocol SignInViewModelDelegate:NSObjectProtocol {
    func signInProceedWith(user:Any)
    func signInProceedWith(error:Error)
}

struct SignInViewModel {
    
    weak var delegate:SignInViewModelDelegate?
    
    func signIn(with email:String,
                password:String) {
        let request = SignInRequest { (result) in
            switch result {
            case .success(let response):
                self.parseResponse(response)
            case .error(let error):
                self.delegate?.signInProceedWith(error: error)
            }
        }
        request.email = email
        request.password = password
        request.perform()
    }
    
    private func parseResponse(_ response: AnyObject) {
        let json = JSON(response)
        guard let token = json["token"].string else {
            parseError(json)
            return
        }
        APIService.service.authToken = token
        self.delegate?.signInProceedWith(user: response)
    }
    
    private func parseError(_ json: JSON) {
        let errorMessage = json["error"].stringValue
        let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : errorMessage])
        self.delegate?.signInProceedWith(error: error)
    }
}
