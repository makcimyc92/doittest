//
//  SignInViewController.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 20.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    
    private var viewModel:SignInViewModel!
    
    var didSignIn:()->() = { }
    var onSignUpBtnPressed:()->() = { }
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = SignInViewModel(delegate: self)
    }

    @IBAction func signUpBtnTouchUpInside(_ sender: Any) {
        onSignUpBtnPressed()
    }
    
    @IBAction func signInBtnTouchUpInside(_ sender: Any) {
        guard let email = emailTextField.text,
            let password = passwordTextField.text else {
            print("unable to get all data for registration")
            return
        }
        self.showLoad()
        viewModel.signIn(with: email, password: password)
    }
}

extension SignInViewController: AlertRenderer { }
extension SignInViewController: HUDRenderer { }
extension SignInViewController:SignInViewModelDelegate {
    
    func signInProceedWith(user:Any) {
        self.hideLoad()
        self.didSignIn()
    }
    
    func signInProceedWith(error:Error) {
        self.hideLoad()
        displayError(error)
    }
}

extension SignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
}
