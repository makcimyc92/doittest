//
//  AddImageViewController.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 21.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import UIKit
import Photos

class AddImageViewController: UIViewController {
    
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var hashtagTextField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    var imageAdded:()->() = { }
    
    private var viewModel:AddImageViewModel!
    
    var coordinate:CLLocationCoordinate2D?

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = AddImageViewModel(delegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        LocationService.shared.startUpdatingLocation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        LocationService.shared.stopUpdatingLocation()
    }
    
    @IBAction func tapOnImage(_ sender: Any) {
        requestToAccessLibrary()
    }
    
    func requestToAccessLibrary() {
        PHPhotoLibrary.requestAuthorization({ (newStatus) in
            if newStatus ==  PHAuthorizationStatus.authorized {
                self.presentImagePickerController()
            } else {
                self.showAlertToSettings("", msg: "Go to setting for get access to library")
            }
        })
    }
    
    func presentImagePickerController() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.modalPresentationStyle = .overFullScreen
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func donePressed(_ sender: Any) {
        guard let image = imageView.image else {
            displayMessage("", msg: "Empty image")
            return
        }
        if self.coordinate == nil {
            self.coordinate = LocationService.shared.currentLocation?.coordinate
        }
        
        guard let coordinate = self.coordinate else {
            self.showAlertToSettings("", msg: "Go to settings for enable geolocation")
            return
        }
        self.showLoad()
        viewModel.addImage(image,
                           descriptionImage: descriptionTextField.text!,
                           hashtag: hashtagTextField.text!,
                           latitude: coordinate.latitude,
                           longitude: coordinate.longitude)
    }
}

extension AddImageViewController:AlertRenderer {}
extension AddImageViewController:HUDRenderer {}
extension AddImageViewController:AlertToSettingsRenderer {}

extension AddImageViewController:AddImageViewModelDelegate {
    func imagesAddSuccess() {
        self.hideWithSuccess()
        imageAdded()
    }
    
    func imageAddProceedWith(error: Error) {
        self.hideLoad()
        displayError(error)
    }
}

extension AddImageViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.imageView.image = image
        if let asset = info[UIImagePickerControllerPHAsset] as? PHAsset,
            let location = asset.location {
            self.coordinate = location.coordinate
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
