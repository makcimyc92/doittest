//
//  AddImageViewModel.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 22.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol AddImageViewModelDelegate:NSObjectProtocol {
    func imagesAddSuccess()
    func imageAddProceedWith(error:Error)
}

struct AddImageViewModel {
    
    weak var delegate:AddImageViewModelDelegate?
    
    func addImage(_ image:UIImage, descriptionImage:String, hashtag:String, latitude:Double, longitude:Double) {
        let request = AddImageRequest { (result) in
            switch result {
            case .success(let response):
                self.parseResponse(response)
            case .error(let error):
                self.delegate?.imageAddProceedWith(error: error)
            }
        }
        request.hashtag = hashtag
        request.descriptionImage = descriptionImage
        request.image = image
        request.latitude = latitude
        request.longitude = longitude
        request.perform()
    }
    
    private func parseResponse(_ response: AnyObject) {
        let json = JSON(response)
        guard let _ = json["bigImage"].string else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Error add image"])
            self.delegate?.imageAddProceedWith(error: error)
            return
        }
        self.delegate?.imagesAddSuccess()
    }
}
