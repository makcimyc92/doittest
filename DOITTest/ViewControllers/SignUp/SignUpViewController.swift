//
//  SignUpViewController.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 20.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import UIKit
import Photos

class SignUpViewController: UIViewController {
    
    private var viewModel:SignUpViewModel!
    var didSignUp:()->() = { }
 
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var userAvatar: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = SignUpViewModel(delegate: self)
    }
    
    @IBAction func signUpBtnTouchUpInside(_ sender: Any) {
        guard let email = emailTextField.text,
            let password = passwordTextField.text,
            let username = usernameTextField.text else {
                print("unable to get all data for registration")
                return
        }
        self.showLoad()
        viewModel.signUp(with: email, password: password, username: username, avatar: userAvatar.image)
    }
    
    @IBAction func tapOnUserAvatar(_ sender: Any) {
        presentImagePickerController()
    }
    
    
    func presentImagePickerController() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        self.present(imagePickerController, animated: true, completion: nil)
    }
}

extension SignUpViewController: AlertRenderer { }
extension SignUpViewController: AlertToSettingsRenderer { }
extension SignUpViewController: HUDRenderer { }

extension SignUpViewController:SignUpViewModelDelegate {
    
    func signUpProceedWith(user:Any) {
        self.hideLoad()
        self.didSignUp()
    }
    
    func signUpProceedWith(error:Error) {
        self.hideLoad()
        displayError(error)
    }
}

extension SignUpViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.userAvatar.image = image
        picker.dismiss(animated: true, completion: nil)
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case usernameTextField:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
}
