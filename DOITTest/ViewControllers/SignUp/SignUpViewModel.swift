//
//  SignUpViewModel.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 20.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol SignUpViewModelDelegate:NSObjectProtocol {
    func signUpProceedWith(user:Any)
    func signUpProceedWith(error:Error)
}

struct SignUpViewModel {
    
    weak var delegate:SignUpViewModelDelegate?
    
    func signUp(with email:String,
                password:String,
                username:String,
                avatar:UIImage?) {
        let request = SignUpRequest { (result) in
            switch result {
            case .success(let response):
                self.parseResponse(response)
            case .error(let error):
                self.delegate?.signUpProceedWith(error: error)
            }
        }
        request.email = email
        request.password = password
        request.username = username
        request.avatar = avatar
        request.perform()
    }
    
    private func parseResponse(_ response: AnyObject) {
        let json = JSON(response)
        guard let token = json["token"].string else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Incorrect values"])
            self.delegate?.signUpProceedWith(error: error)
            return
        }
        APIService.service.authToken = token
        self.delegate?.signUpProceedWith(user: response)
    }
}
