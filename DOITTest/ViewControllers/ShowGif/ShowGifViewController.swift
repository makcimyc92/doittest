//
//  ShowGifViewController.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 21.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import UIKit

class ShowGifViewController: UIViewController {

    @IBOutlet weak var gifImageView: UIImageView!
    private var viewModel:ShowGifViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        self.showLoad()
        viewModel = ShowGifViewModel(delegate: self)
        viewModel.getGifURL()
        // Do any additional setup after loading the view.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func configure() {
        modalPresentationStyle = .fullScreen
        modalTransitionStyle = .crossDissolve
        modalPresentationCapturesStatusBarAppearance = true
    }
    
    @IBAction func tapOnSelf(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}


extension ShowGifViewController:HUDRenderer { }

extension ShowGifViewController: ShowGifViewModelDelegate{
    func getGif(_ url: String) {
        self.hideLoad()
        self.gifImageView.image = UIImage.gifImageWithURL(gifUrl: url)
    }
    
    func getGifURLWith(error: Error) {
        self.hideLoad()
    }
    
    
}
