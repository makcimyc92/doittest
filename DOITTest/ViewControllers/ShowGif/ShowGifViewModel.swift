//
//  ShowGifViewModel.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 26.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ShowGifViewModelDelegate:NSObjectProtocol {
    func getGif(_ url:String)
    func getGifURLWith(error:Error)
}

struct ShowGifViewModel {
    
    weak var delegate:ShowGifViewModelDelegate?
    
    func getGifURL() {
        let request = ShowGifRequest { (result) in
            switch result {
            case .success(let response):
                self.parseResponse(response)
                print(response)
            case .error(let error):
                print(error)
            }
        }
        request.weather = ""
        request.perform()
    }
    
    private func parseResponse(_ response: AnyObject) {
        let json = JSON(response)
        guard let url = json["gif"].string else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Error get gif URL"])
            self.delegate?.getGifURLWith(error: error)
            return
        }
        self.delegate?.getGif(url)
    }
}
