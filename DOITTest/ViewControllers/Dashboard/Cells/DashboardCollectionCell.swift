//
//  DashboardCollectionCell.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 21.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import UIKit

class DashboardCollectionCell: UICollectionViewCell {
    
    static let fontAddress = UIFont.systemFont(ofSize: 13)
    static let fontWeather = UIFont.systemFont(ofSize: 16)
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var weatherLabel: UILabel! {
        didSet{
            weatherLabel.font = DashboardCollectionCell.fontWeather
        }
    }
    @IBOutlet weak var addressLabel: UILabel! {
        didSet{
            addressLabel.font = DashboardCollectionCell.fontAddress
        }
    }
    
    @IBOutlet weak var textsStackView: UIStackView!
    
    var imageModel:ImageModel!

    func configureFrom(_ imageModel:ImageModel) {
        self.imageModel = imageModel
        weatherLabel.text = imageModel.weather
        addressLabel.text = imageModel.address
        imageView.image = imageModel.image
    }
}
