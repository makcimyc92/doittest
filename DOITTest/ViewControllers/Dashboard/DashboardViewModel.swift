//
//  DashboardViewModel.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 21.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol DashboardViewModelDelegate:NSObjectProtocol {
    func imagesGetSuccess(images:[ImageModel])
    func imagesGetProceedWith(error:Error)
}

struct DashboardViewModel {
    
    weak var delegate:DashboardViewModelDelegate?
    
    func getAllImage() {
        GetAllRequest { (result) in
            switch result {
            case .success(let response):
                self.parseResponse(response)
            case .error(let error):
                self.delegate?.imagesGetProceedWith(error: error)
            }
        }.perform()
    }
    
    private func parseResponse(_ response: AnyObject) {
        let json = JSON(response)
        guard let images = json["images"].array else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Error get image"])
            self.delegate?.imagesGetProceedWith(error: error)
            return;
        }
        let imageModels = images.map({ (imageDict) -> ImageModel in
            return ImageModel(from: imageDict)
        })
        self.delegate?.imagesGetSuccess(images: imageModels)
    }
}
