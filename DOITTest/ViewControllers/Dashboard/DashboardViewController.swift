//
//  DashboardViewController.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 20.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {

    var onLogoutBtnPressed:()->() = { }
    
    private var viewModel:DashboardViewModel!
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            if let layout = collectionView.collectionViewLayout as? ImageLayout {
                layout.delegate = self
            }
        }
    }
    
    var imageModels:[ImageModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = DashboardViewModel(delegate: self)
        viewModel.getAllImage()
    }
    
    @IBAction func logout(_ sender: Any) {
        onLogoutBtnPressed()
    }
    
    @IBAction func addImage(_ sender: Any) {
        let controller:AddImageViewController = UIStoryboard(storyboard:UIStoryboard.Storyboard.Main).instantiateViewController()
        controller.imageAdded = imageAdded
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func showImage(_ sender: Any) {
        let controller:ShowGifViewController = UIStoryboard(storyboard:UIStoryboard.Storyboard.Main).instantiateViewController()
        present(controller, animated: true, completion: nil)
    }
    
    func imageAdded() {
        viewModel.getAllImage()
    }
}

extension DashboardViewController:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! DashboardCollectionCell
        let imageModel = imageModels[indexPath.row]
        cell.configureFrom(imageModel)
        return cell
    }
}

extension DashboardViewController: ImageLayoutDelegate {
    func collectionView(_ collectionView:UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
        guard let layout = collectionView.collectionViewLayout as? ImageLayout else {
            return 0
        }
        let model = imageModels[indexPath.row]
        let heightAddress = model.address.height(withConstrainedWidth: collectionView.frame.width/2, font: DashboardCollectionCell.fontAddress)
        let heightWeather = model.weather.height(withConstrainedWidth: collectionView.frame.width/2, font: DashboardCollectionCell.fontWeather)
        let heightText = heightAddress + heightWeather
        guard let image = model.image else {
            return heightText
        }
        let ratio = image.size.height / image.size.width
        return layout.widthCell * ratio + heightText
    } 
}

extension DashboardViewController:AlertRenderer {}

extension DashboardViewController:DashboardViewModelDelegate {
    func imagesGetSuccess(images: [ImageModel]) {
        self.imageModels = images
        images.forEach { (model) in
            model.delegate = self
        }
        self.collectionView.reloadData()
    }
    
    func imagesGetProceedWith(error:Error) {
        displayError(error)
    }
}

extension DashboardViewController:ImageModelDelegate {
    func imagesGetSuccess() {
        self.collectionView.reloadData()
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
}
