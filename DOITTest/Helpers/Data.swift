//
//  Data.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 9/20/17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import Foundation

extension Data {
    var mimeType: String {
        let array = [UInt8](self)
        let ext: String
        switch (array[0]) {
        case 0xFF:
            ext = "image/jpeg"
        case 0x89:
            ext = "image/png"
        case 0x47:
            ext = "image/gif"
        case 0x49, 0x4D :
            ext = "image/tiff"
        default:
            ext = "unknown"
        }
        return ext
    }
}

extension Data {
    
    init<T>(from value: T) {
        var value = value
        self.init(buffer: UnsafeBufferPointer(start: &value, count: 1))
    }
    
    func to<T>(type: T.Type) -> T {
        return self.withUnsafeBytes { $0.pointee }
    }
}
