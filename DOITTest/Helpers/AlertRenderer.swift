//
//  AlertRenderer.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 20.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import UIKit

protocol AlertRenderer {
    func displayMessage(_ title: String, msg: String)
    func displayError(_ error: Error)
}

extension AlertRenderer where Self: UIViewController {
    func displayError(_ error: Error) {
        displayMessage("Error!", msg: error.localizedDescription)
    }
    
    func displayMessage(_ title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel) { (action) -> Void in
            alertController.dismiss(animated: true, completion:nil)
        }
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
}

protocol AlertToSettingsRenderer {
    func showAlertToSettings(_ title: String, msg: String)
}

extension AlertToSettingsRenderer where Self: UIViewController {
    func showAlertToSettings(_ title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        let goSettings = UIAlertAction(title: "Setting", style: .default) { (action) in
            UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!, completionHandler: nil)
        }
        alertController.addAction(action)
        alertController.addAction(goSettings)
        present(alertController, animated: true, completion: nil)
    }
}

protocol HUDRenderer {
    func showLoad()
    func hideLoad()
    
}

extension HUDRenderer where Self: UIViewController {
    func showLoad() {
        self.pleaseWait()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func hideWithSuccess() {
        self.hideLoad()
        self.noticeSuccess("", autoClear: true, autoClearTime: 1)
    }
    
    func hideLoad() {
        self.clearAllNotice()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
}



