//
//  DOIT.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 20.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import UIKit

class DOIT {
    
    private var rootNavController: UINavigationController!
    private let authStoryboard = UIStoryboard(storyboard:UIStoryboard.Storyboard.Auth)
    private let mainStoryboard = UIStoryboard(storyboard:UIStoryboard.Storyboard.Main)
    
    var dashboardViewController:DashboardViewController {
        let controller:DashboardViewController = mainStoryboard.instantiateViewController()
        controller.onLogoutBtnPressed = logout
        return controller
    }
    
    var signupViewController:SignUpViewController {
        let controller:SignUpViewController = authStoryboard.instantiateViewController()
        controller.didSignUp = enterApp
        return controller
    }
    
    var signinViewController:SignInViewController {
        let controller:SignInViewController = authStoryboard.instantiateViewController()
        controller.onSignUpBtnPressed = {
            self.rootNavController.pushViewController(self.signupViewController, animated: true)
        }
        controller.didSignIn = enterApp
        return controller
    }
    
    init(window: UIWindow) {
        rootNavController = window.rootViewController as? UINavigationController
        configure()
    }
    
    
    private func configure() {
        if !APIService.service.authToken.isEmpty {
            enterApp()
        } else {
            presentAuthController()
        }
    }
    
    private func logout() {
        APIService.service.authToken = ""
        presentAuthController()
    }
    
    private func enterApp() {
        DispatchQueue.main.async {
            self.rootNavController.isNavigationBarHidden = false
            self.rootNavController.setViewControllers([self.dashboardViewController], animated: true)
        }
    }
    
    private func presentAuthController() {
        rootNavController.setViewControllers([signinViewController], animated: false)
    }
    
//    private func pushAddImageController() {
//        rootNavController.pushViewController(self.addImageController, animated: true)
//    }
    
}

