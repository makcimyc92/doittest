//
//  ImageModel.swift
//  DOITTest
//
//  Created by Max Vasilevsky on 21.09.17.
//  Copyright © 2017 Max Vasilevsky. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ImageModelDelegate:NSObjectProtocol {
    func imagesGetSuccess()
}

class ImageModel {
    var imagePath = ""
    var address = ""
    var weather = ""
    weak var delegate:ImageModelDelegate?
    
    private var _image: UIImage?
    private var isDownloadImage = false
    
    var image:UIImage? {
        get {
            guard let image = _image else {
                loadImage()
                return nil
            }
            return image
        }
    }
    
    init(from json:JSON) {
        imagePath = json["bigImagePath"].string ?? ""
        if let parameters = json["parameters"].dictionary {
            address = parameters["address"]?.string ?? ""
            weather = parameters["weather"]?.string ?? ""
        }
    }
    
    func loadImage() {
        if isDownloadImage {
            return
        }
        if let url = URL(string: imagePath){
            isDownloadImage = true
            weak var weakSelf = self
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url)
                print(Date())
                weakSelf?._image = data != nil ? UIImage(data: data!) : UIImage(named: "no_image")
                weakSelf?.isDownloadImage = false
                DispatchQueue.main.async {
                    weakSelf?.delegate?.imagesGetSuccess()
                }
            }
        }
    }
    
}
